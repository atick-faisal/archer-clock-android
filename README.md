# ArcherClock
An android app to control archery game timer clock wirelessly over WiFi
Normal Mode: 
![picture](Screenshot_2018-04-03-20-04-53.png) 
Alternate Mode: 
![picture](Screenshot_2018-04-03-20-05-36.png) 
![picture](Screenshot_2018-04-03-20-05-29.png) 
Settings: 
![picture](Screenshot_2018-04-03-20-05-43.png) 
